;
var page = 1;
(function ($) {
    $(document).ready(function () {

        $("#statuses").on("click","h1",function(){
            alert($(this).html());
        });

        $("#poststatus").on("click", function (e) {
            e.preventDefault();
            var status = $("#status").val();
            var nonce = $("#nonce").val();
            var file = $("#image")[0].files[0];
            console.log(status, file);
            if (file == undefined) {
                var params = {
                    s: status,
                    n: nonce,
                    action: 'status'
                }

                $.post(cls12.admin_ajax, params, function (data) {
                    if (data) {
                        $("#statuses").prepend(data);
                    }
                });
            }else{
                var form = new FormData();
                //$_FILES['image']
                form.append("image",file);
                form.append("s",status);
                form.append("n",nonce);
                form.append("action","status");

                $.ajax({
                    url:cls12.admin_ajax,
                    data:form,
                    type:'POST',
                    processData:false,
                    contentType:false,
                    success:function(data){
                        if (data) {
                            $("#statuses").prepend(data);
                        }
                    }
                });
            }



            //document.getElementById("image");
        });

        $("#loadmore").on("click",function(){
            var current_page = location.href;
            page++;
            alert(current_page+"page/"+page+"/");
            $.get(current_page+"page/"+page+"/",{},function(data){
                if(data){
                    $("#statuses").append(data);
                }else{
                    $("#loadmore").hide();
                }
            });
        });
    });
})(jQuery);






